//
//  UIImageView+FTLFlickLoading.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface UIImageView (FTLFlickLoading)

- (void)setImageWithURL:(NSURL *)url
                success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *  response, UIImage *image))success
                failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *  response, NSError *error))failure;

@end
