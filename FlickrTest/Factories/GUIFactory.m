//
//  GUIFactory.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "GUIFactory.h"
#import "FLTFlickrImageGalleryController.h"
#import "FLTApplicationSettingsViewController.h"
#import "FTLApplicationModel.h"
#import "FTLFlickrImage.h"
#import "FTLDetailedImageViewController.h"

@implementation GUIFactory

- (instancetype)initWithApplicationModel:(FTLApplicationModel *)appModel
{
    self = [super init];
    
    if(self)
    {
        _applicationModel = appModel;
        [_applicationModel setupModels];
    }
    
    return self;
}

- (UIViewController *)createInitialViewController
{
    UITabBarController *tabBarController = [UITabBarController new];
    
    UITabBarItem *galleryItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:0];
    UITabBarItem *settingsItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:1];
    
    UIViewController *gallery = [self createFlickrGalleryViewController];
    gallery.tabBarItem = galleryItem;
    
    UIViewController *settings = [self createApplicationSettingsViewController];
    settings.tabBarItem = settingsItem;
    
    tabBarController.viewControllers = @[gallery,settings];
    tabBarController.view.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    
    return tabBarController;
}

- (UIViewController *)createFlickrGalleryViewController
{
    UICollectionViewFlowLayout *galleryFlowLayout = [UICollectionViewFlowLayout new];
    galleryFlowLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    galleryFlowLayout.minimumLineSpacing = 2.0;
    galleryFlowLayout.minimumInteritemSpacing = 2.0;
    
    FLTFlickrImageGalleryController *imageGallery = [[FLTFlickrImageGalleryController alloc] initWithCollectionViewLayout:galleryFlowLayout];
    
    imageGallery.settings = self.applicationModel.settings;
    imageGallery.imageDowloader = [self.applicationModel createImageDowloaderWithResultHandler:imageGallery];
    imageGallery.factory = self;
    
    UINavigationController *galleryNavigationController = [[UINavigationController alloc] initWithRootViewController:imageGallery];
    
    return galleryNavigationController;
}

- (UIViewController *)createApplicationSettingsViewController
{
    FLTApplicationSettingsViewController *applicationSettings = [[self storyboard]
                                                                 instantiateViewControllerWithIdentifier:@"ApplicationSettings"];
    
    applicationSettings.settings = self.applicationModel.settings;
    
    return applicationSettings;
}

- (UIViewController *)createDetailedViewControllerForImage:(FTLFlickrImage *)image
{
    FTLDetailedImageViewController *detailed = [[self storyboard]
                                                                 instantiateViewControllerWithIdentifier:@"DetailedImageViewController"];
    detailed.flickrImage = image;
    
    return detailed;
}

#pragma mark - Private methods -

- (UIStoryboard *)storyboard
{
    return [UIStoryboard storyboardWithName:@"FlickrGUI" bundle:nil];
}

@end
