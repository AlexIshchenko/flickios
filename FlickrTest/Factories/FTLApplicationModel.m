//
//  FTLApplicationModel.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FTLApplicationModel.h"

@implementation FTLApplicationModel

- (void)setupModels
{
    _settings = [self createApplicationSettings];
}

- (FTLApplicationSettings *)createApplicationSettings
{
    return [FTLApplicationSettings new];
}

- (FTLFlickrImageDowloader *)createImageDowloaderWithResultHandler:(id<FTLFlickrImageDowloaderResultHandler>)resultHandler
{
    return [[FTLFlickrImageDowloader alloc] initWithResultHandler:resultHandler];
}

@end
