//
//  GUIFactory.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class FTLApplicationModel;
@class FTLFlickrImage;

@interface GUIFactory : NSObject

@property(nonatomic,strong,readonly) FTLApplicationModel *applicationModel;

- (instancetype)init __unavailable;

- (instancetype)initWithApplicationModel:(FTLApplicationModel *)appModel NS_DESIGNATED_INITIALIZER;

- (UIViewController *)createInitialViewController;

- (UIViewController *)createFlickrGalleryViewController;

- (UIViewController *)createApplicationSettingsViewController;

- (UIViewController *)createDetailedViewControllerForImage:(FTLFlickrImage *)image;

@end
