//
//  FTLApplicationModel.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTLFlickrImageDowloader.h"
#import "FTLApplicationSettings.h"


@interface FTLApplicationModel : NSObject

@property(nonatomic,strong,readonly) FTLApplicationSettings *settings;
@property(nonatomic,strong,readonly) FTLFlickrImageDowloader *imageDownloader;

- (void)setupModels;

- (FTLApplicationSettings *)createApplicationSettings;
- (FTLFlickrImageDowloader *)createImageDowloaderWithResultHandler:(id<FTLFlickrImageDowloaderResultHandler>)resultHandler;

@end
