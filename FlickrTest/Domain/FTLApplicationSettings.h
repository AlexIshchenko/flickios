//
//  FTLApplicationSettings.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTLApplicationSettings : NSObject

@property(nonatomic,assign,readonly) NSUInteger numberOfColumnsInGallery;

- (void)changeNumberOfColumnsToValue:(NSUInteger)newValue;

@end
