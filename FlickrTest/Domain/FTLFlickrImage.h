//
//  FTLFlickrImage.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTLFlickrImage : NSObject

- (NSURL *)middleImageURL;
- (NSURL *)bigImageURL;

- (instancetype)init __unavailable;

- (instancetype)initWithID:(NSString *)imageId farm:(NSString *)farm server:(NSString *)server secret:(NSString *)secret NS_DESIGNATED_INITIALIZER;

+ (FTLFlickrImage *)createImageForID:(NSString *)imageId farm:(NSString *)farm server:(NSString *)server secret:(NSString *)secret;

@end
