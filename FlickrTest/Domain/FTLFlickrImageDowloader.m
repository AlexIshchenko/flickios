//
//  FTLFlickrImageDowloader.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FTLFlickrImageDowloader.h"
#import "FTLFlickrError.h"
#import "FTLFlickrImage.h"
#import <AFNetworking/AFNetworking.h>

static NSString *const kApiKey = @"1707c933f244b97aabdd25335f0637d8";

@interface FTLFlickrImageDowloader()
{
    NSMutableArray<FTLFlickrImage *> *_images;
    NSUInteger _page;
    FTLFlickrImageDowloaderOptions *_currentOptions;
}

@end

@implementation FTLFlickrImageDowloader

- (instancetype)initWithResultHandler:(id<FTLFlickrImageDowloaderResultHandler>)resultHandler
{
    self = [super init];
    if(self)
    {
        _resultHandler = resultHandler;
        _images = [NSMutableArray arrayWithCapacity:300];
        _page = 1;
    }
    return self;
}

- (void)downloadImagesForOptions:(FTLFlickrImageDowloaderOptions *)options;
{
    _currentOptions = options;
    [self resetSearchResults];
    [self performNewSearchResultForOptions:_currentOptions];
}

- (void)dowloadNewBatch
{
    @synchronized (self) {
        _page++;
        [self performNewSearchResultForOptions:_currentOptions];
    }
}

- (void)resetSearchResults
{
    _page = 1;
    @synchronized (self) {
        [_images removeAllObjects];
    }
}

- (void)performNewSearchResultForOptions:(FTLFlickrImageDowloaderOptions *)options
{
    NSURL *requestURL = [self urlForOptions:options];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:requestURL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [self processRespone:responseObject];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self notifyHandlerWithError:[self flickErrorForSessionManagerError:error]];
    }];
}

- (FTLFlickrImage *)imageForIndex:(NSUInteger)index
{
    if(index<_images.count)
    {
        @synchronized (self)
        {
            return _images[index];
        }
    }
    else
    {
        return nil;
    }
}

- (NSUInteger)numberOfImages
{
    NSUInteger numberOfImages = _images.count;
    return numberOfImages;
}

#pragma mark - Private methods -

- (void)processRespone:(NSDictionary *)response
{
    if([self isResponseSuccessful:response])
    {
        [self processSuccessfulResponse:response[@"photos"]];
    }
    else
    {
        [self processBadResponse:response];
    }
}

- (void)processSuccessfulResponse:(NSDictionary *)response
{
    NSArray<FTLFlickrImage *> *newImages = [self mapResponse:response];
    @synchronized (self) {
        [_images addObjectsFromArray:newImages];
    }
    [self notifyHandlerWithImages:newImages];
}

- (void)processBadResponse:(NSDictionary *)response
{
    [self notifyHandlerWithError:[self errorForResponse:response]];
}

- (BOOL)isResponseSuccessful:(NSDictionary *)response
{
    if([response[@"stat"] isEqualToString:@"ok"])
        return YES;
    else
        return NO;
}

- (FTLFlickrError *)errorForResponse:(NSDictionary *)response
{
    FTLFlickrError *newError = [FTLFlickrError new];
    newError.flickrMessage = response[@"message"];
    
    return newError;
}

- (NSArray<FTLFlickrImage *> *)mapResponse:(NSDictionary *)response
{
    NSMutableArray<FTLFlickrImage *> *newImages = [NSMutableArray new];
    
    for(NSDictionary *imageDictionary in response[@"photo"])
    {
        NSString *imageID = imageDictionary[@"id"];
        NSString *farm = imageDictionary[@"farm"];
        NSString *server = imageDictionary[@"server"];
        NSString *secret = imageDictionary[@"secret"];
        
        FTLFlickrImage *flickrImage = [FTLFlickrImage createImageForID:imageID farm:farm server:server secret:secret];
        
        if(flickrImage)
        {
            [newImages addObject:flickrImage];
        }
    }
    
    return newImages;
}

- (FTLFlickrError *)flickErrorForSessionManagerError:(NSError *)error
{
    FTLFlickrError  *flickrError = [FTLFlickrError new];
    flickrError.flickrMessage = error.localizedDescription;
    
    return flickrError;
}

- (NSURL *)urlForOptions:(FTLFlickrImageDowloaderOptions *)options
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&text=%@&per_page=%lu&page=%lu&format=json&nojsoncallback=1",
                                       kApiKey,
                                       options.searchString,
                                       (unsigned long)options.fetchBatchSize,
                                       (unsigned long)_page]];
    
    return url;
}

- (void)notifyHandlerWithImages:(NSArray<FTLFlickrImage *> *)newImages
{
    [self.resultHandler imageDowloader:self didDownloadImages:newImages];
}

- (void)notifyHandlerWithError:(FTLFlickrError *)error
{
    [self.resultHandler imageDowloader:self didDownloadWithError:error];
}

@end

@implementation FTLFlickrImageDowloaderOptions

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _searchString = @"";
        _fetchBatchSize = 20;
    }
    return self;
}

@end
