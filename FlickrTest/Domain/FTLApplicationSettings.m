//
//  FTLApplicationSettings.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FTLApplicationSettings.h"

static const NSUInteger kDefaultNumberOfColumns = 2;
static const NSUInteger kMaxColumnNumber = 3;

@implementation FTLApplicationSettings

- (instancetype)init
{
    self = [super init];
    
    if(self)
    {
        _numberOfColumnsInGallery = kDefaultNumberOfColumns;
    }
    
    return self;
}

- (void)changeNumberOfColumnsToValue:(NSUInteger)newValue
{
    if(newValue <= kMaxColumnNumber && newValue>=1)
    {
        [self willChangeValueForKey:@"numberOfColumnsInGallery"];
        _numberOfColumnsInGallery = newValue;
        [self didChangeValueForKey:@"numberOfColumnsInGallery"];
    }
}


@end
