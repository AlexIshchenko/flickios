//
//  FTLFlickrImageDowloader.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTLFlickrImage.h"
#import "FTLFlickrError.h"

@protocol FTLFlickrImageDowloaderResultHandler;
@class FTLFlickrImageDowloaderOptions;

@interface FTLFlickrImageDowloader : NSObject

@property(nonatomic,weak,readonly) id<FTLFlickrImageDowloaderResultHandler> resultHandler;

- (instancetype)init __unavailable;

- (instancetype)initWithResultHandler:(id<FTLFlickrImageDowloaderResultHandler>)resultHandler NS_DESIGNATED_INITIALIZER;

- (void)downloadImagesForOptions:(FTLFlickrImageDowloaderOptions *)options;

- (void)dowloadNewBatch;

- (FTLFlickrImage *)imageForIndex:(NSUInteger)index;

- (NSUInteger)numberOfImages;

@end

@protocol FTLFlickrImageDowloaderResultHandler <NSObject>

- (void)imageDowloader:(FTLFlickrImageDowloader *)imageDownloader didDownloadImages:(NSArray<FTLFlickrImage *> *)images;

- (void)imageDowloader:(FTLFlickrImageDowloader *)imageDownloader didDownloadWithError:(FTLFlickrError *)error;

@end

@interface FTLFlickrImageDowloaderOptions : NSObject

@property(nonatomic,strong) NSString *searchString;
@property(nonatomic,assign) NSUInteger fetchBatchSize;

@end
