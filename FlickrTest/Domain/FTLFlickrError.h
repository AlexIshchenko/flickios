//
//  FTLFlickrError.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTLFlickrError : NSError

@property(nonatomic,strong) NSString *flickrMessage;

@end
