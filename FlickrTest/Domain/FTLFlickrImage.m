//
//  FTLFlickrImage.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FTLFlickrImage.h"

@interface FTLFlickrImage()
{
    NSString *_imageId;
    NSString *_farm;
    NSString *_server;
    NSString *_secret;
}

@end

@implementation FTLFlickrImage

- (instancetype)initWithID:(NSString *)imageId farm:(NSString *)farm server:(NSString *)server secret:(NSString *)secret
{
    self = [super init];
    if(self)
    {
        NSParameterAssert(imageId);
        NSParameterAssert(farm);
        NSParameterAssert(server);
        NSParameterAssert(secret);
        
        _imageId = imageId;
        _farm = farm;
        _server = server;
        _secret = secret;
        
    }
    return self;
}

- (NSURL *)middleImageURL
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@_m.jpg",
            _farm,
            _server,
            _imageId,
            _secret]];
}

- (NSURL *)bigImageURL
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@_b.jpg",
                                 _farm,
                                 _server,
                                 _imageId,
                                 _secret]];
}

+ (FTLFlickrImage *)createImageForID:(NSString *)imageId farm:(NSString *)farm server:(NSString *)server secret:(NSString *)secret
{
    if(imageId == nil || farm == nil || server == nil || secret == nil)
    {
        return nil;
    }
    
    return [[FTLFlickrImage alloc] initWithID:imageId farm:farm server:server secret:secret];
}

@end
