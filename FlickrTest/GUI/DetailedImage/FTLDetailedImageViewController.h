//
//  FTLDetailedImageViewController.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTLFlickrImage.h"

@interface FTLDetailedImageViewController : UIViewController

@property(nonatomic,strong) IBOutlet UIImageView *imageView;
@property(nonatomic,strong) FTLFlickrImage *flickrImage;

@end
