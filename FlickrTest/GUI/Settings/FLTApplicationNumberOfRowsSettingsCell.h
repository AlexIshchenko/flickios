//
//  FLTApplicationNumberOfRowsSettingsCell.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FLTApplicationNumberOfRowsSettingsCellDelegate;

@interface FLTApplicationNumberOfRowsSettingsCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UILabel *numberOfColumnsLabel;
@property(nonatomic,weak) IBOutlet UIStepper *numberOfColumnsStepper;

@property(nonatomic,weak) id<FLTApplicationNumberOfRowsSettingsCellDelegate> delegate;

@property(nonatomic,assign) NSUInteger numberOfColumns;

@end

@protocol FLTApplicationNumberOfRowsSettingsCellDelegate <NSObject>

- (void)applicationNumberOfRowsSettingsCell:(FLTApplicationNumberOfRowsSettingsCell *)cell didChangeValue:(NSUInteger)newValue;

@end
