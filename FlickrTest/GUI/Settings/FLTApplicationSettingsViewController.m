//
//  FLTApplicationSettingsViewController.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FLTApplicationSettingsViewController.h"
#import "FLTApplicationNumberOfRowsSettingsCell.h"

static NSString * const kNumberOfColumnsCellIdentifier = @"settingsCellIdentifier";

@interface FLTApplicationSettingsViewController ()<FLTApplicationNumberOfRowsSettingsCellDelegate>

@end

@implementation FLTApplicationSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FLTApplicationNumberOfRowsSettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:kNumberOfColumnsCellIdentifier];
    cell.delegate = self;
    cell.numberOfColumns = self.settings.numberOfColumnsInGallery;
    
    return cell;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Flickr Image Gallery Settings";
}

#pragma mark - FLTApplicationNumberOfRowsSettingsCellDelegate -

- (void)applicationNumberOfRowsSettingsCell:(FLTApplicationNumberOfRowsSettingsCell *)cell didChangeValue:(NSUInteger)newValue
{
    [self.settings changeNumberOfColumnsToValue:newValue];
    
    cell.numberOfColumns = self.settings.numberOfColumnsInGallery;
}

@end
