//
//  FLTApplicationNumberOfRowsSettingsCell.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FLTApplicationNumberOfRowsSettingsCell.h"

@implementation FLTApplicationNumberOfRowsSettingsCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setNumberOfColumns:(NSUInteger)numberOfColumns
{
    _numberOfColumns = numberOfColumns;
    
    self.numberOfColumnsStepper.value = numberOfColumns;
    
    self.numberOfColumnsLabel.text = [NSString stringWithFormat:@"Number of columns: %lu",(unsigned long)_numberOfColumns];
}

- (IBAction)stepperValueChanged:(UIStepper *)sender
{
    [self.delegate applicationNumberOfRowsSettingsCell:self didChangeValue:[sender value]];
}

@end
