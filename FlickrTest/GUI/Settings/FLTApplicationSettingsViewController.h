//
//  FLTApplicationSettingsViewController.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTLApplicationSettings.h"

@interface FLTApplicationSettingsViewController : UITableViewController

@property(nonatomic,strong) FTLApplicationSettings *settings;

@end
