//
//  FTLGalleryCollectionViewCell.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FTLGalleryCollectionViewCell.h"

@implementation FTLGalleryCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        
        self.backgroundColor = [UIColor darkGrayColor];
        
        [_imageView addSubview:_indicatorView];
        [self.contentView addSubview:_imageView];
        _indicatorView.center = _imageView.center;
    }
    return self;
}


- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.imageView.image = nil;
    [self.indicatorView stopAnimating];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = self.contentView.bounds;
    self.indicatorView.center = self.imageView.center;
}

@end
