//
//  FTLGalleryCollectionViewCell.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/6/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FTLGalleryCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIActivityIndicatorView *indicatorView;

@end
