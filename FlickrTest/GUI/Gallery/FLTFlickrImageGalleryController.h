//
//  FLTFlickrImageGalleryController.h
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTLFlickrImageDowloader.h"
#import "FTLApplicationSettings.h"
#import "GUIFactory.h"

@interface FLTFlickrImageGalleryController : UICollectionViewController<FTLFlickrImageDowloaderResultHandler>

@property(nonatomic,strong) FTLFlickrImageDowloader *imageDowloader;
@property(nonatomic,strong) FTLApplicationSettings *settings;

@property(nonatomic,strong) GUIFactory *factory;

@end
