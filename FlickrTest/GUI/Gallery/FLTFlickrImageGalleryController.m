//
//  FLTFlickrImageGalleryController.m
//  FlickrTest
//
//  Created by Alex Ishchenko on 2/3/17.
//  Copyright © 2017 Alex Ishchenko. All rights reserved.
//

#import "FLTFlickrImageGalleryController.h"
#import "FTLGalleryCollectionViewCell.h"
#import "UIImageView+FTLFlickLoading.h"

@interface FLTFlickrImageGalleryController ()<UISearchBarDelegate>
{
    CGSize _cellSize;
}

@property(nonatomic,strong) UISearchBar *searchBar;
@property(nonatomic,strong) UIActivityIndicatorView *spinner;

@end

@implementation FLTFlickrImageGalleryController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupCollectionView];
    [self subscribeForColumnsNumberObservation];
    [self setupSearchController];
    [self setupSpinnerView];
    [self loadImages];
}

- (void)loadImages
{
    FTLFlickrImageDowloaderOptions *initialOptions = [FTLFlickrImageDowloaderOptions new];
    initialOptions.searchString = @"animals";
    
    [self.imageDowloader downloadImagesForOptions:initialOptions];
    
    [self showSpinner];
}

- (void)loadImagesForText:(NSString *)searchText
{
    FTLFlickrImageDowloaderOptions *options = [FTLFlickrImageDowloaderOptions new];
    options.searchString = searchText;
    
    [self.imageDowloader downloadImagesForOptions:options];
}

#pragma mark - UICollectionViewDataSource -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.imageDowloader numberOfImages];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FTLGalleryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    FTLFlickrImage *image = [self.imageDowloader imageForIndex:indexPath.row];
    
    __weak typeof(FTLGalleryCollectionViewCell *) weakCell = cell;
    
    [weakCell.indicatorView startAnimating];
    
    [cell.imageView setImageWithURL:[image middleImageURL] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [weakCell.indicatorView stopAnimating];
        [weakCell.imageView setImage:image];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         [weakCell.indicatorView stopAnimating];
    }];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return _cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *detailedController = [self.factory createDetailedViewControllerForImage:[self.imageDowloader imageForIndex:indexPath.row]];
    
    detailedController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:detailedController
                                         animated:YES];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self isNeedLoadNextImageBatchForIndexPath:indexPath])
    {
        [self showSpinner];
        [self.imageDowloader dowloadNewBatch];
    }
}

- (BOOL)isNeedLoadNextImageBatchForIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [self.imageDowloader numberOfImages] - 1)
        return YES;
    
    return NO;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    _cellSize = [self calculateSizeForCellFromSize:size];
    [self invalidateLayout];
}

- (void)invalidateLayout
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    [layout invalidateLayout];
}
#pragma mark - UI setup -

- (void)setupCollectionView
{
    _cellSize = [self calculateSizeForCellFromSize:[UIScreen mainScreen].bounds.size];
    [self.collectionView registerClass:[FTLGalleryCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
}

- (CGSize)calculateSizeForCellFromSize:(CGSize)parentSize
{
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionViewLayout;
    
    CGFloat screenWidth = parentSize.width;
    UIEdgeInsets sectionInset = flowLayout.sectionInset;
    CGFloat interitemSpacing = flowLayout.minimumInteritemSpacing;
    CGFloat contentWidth = screenWidth - sectionInset.left - sectionInset.right - 2*interitemSpacing;
    
    CGFloat cellWidth = floor(contentWidth/self.settings.numberOfColumnsInGallery);
    
    return CGSizeMake(cellWidth, cellWidth);
}

- (void)setupSearchController
{
    self.searchBar = [[UISearchBar alloc] init];
    self.searchBar.delegate = self;
    
    self.navigationItem.titleView = self.searchBar;
    [self.searchBar sizeToFit];
}

- (void)setupSpinnerView
{
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.center = self.view.center;
    [self.view addSubview:self.spinner];
    [self.view bringSubviewToFront:self.spinner];
}

- (void)showSpinner
{
    [self.spinner startAnimating];
}

- (void)hideSpinner
{
    [self.spinner stopAnimating];
}

#pragma mark - UISearchBarDelegate -

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar endEditing:YES];
    [self showSpinner];
    [self loadImagesForText:searchBar.text];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

#pragma mark - FTLFlickrImageDowloaderResultHandler -

- (void)imageDowloader:(FTLFlickrImageDowloader *)imageDownloader didDownloadImages:(NSArray<FTLFlickrImage *> *)images
{
    [self hideSpinner];
    [self.collectionView reloadData];
}

- (void)imageDowloader:(FTLFlickrImageDowloader *)imageDownloader didDownloadWithError:(FTLFlickrError *)error
{
    [self hideSpinner];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.flickrMessage preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:NULL];
}

#pragma mark - Value Observation -

- (void)subscribeForColumnsNumberObservation
{
    [self.settings addObserver:self forKeyPath:@"numberOfColumnsInGallery" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"numberOfColumnsInGallery"])
    {
        _cellSize = [self calculateSizeForCellFromSize:[UIScreen mainScreen].bounds.size];
        [self invalidateLayout];
    }
}


@end
